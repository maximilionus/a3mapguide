/*********************************
Все названия звуков шагов можно найти по пути \a3\sounds_f\config.cpp : в классе SoundEnvironExt
	Пример названия: rock[]={***} -> rock, это название звука

soundHit :
	hard_ground
	concrete
	soft_ground
	building
*********************************/
class CfgSurfaces
{
	class Default {};
	class maxmapm_CoastalGrass_surface: Default
	{
		files = "mm_grass_coastal*"; //Путь до текстуры + материала (маской)
		rough = 0.1; //Грубость поверхности
		dust = 1; //Пыльность поверхности
		soundEnviron = "grass"; //Звук поверхности
		character = "maxmapm_Grass_Character"; //Класс поверхности из CfgSurfaceCharacters
		soundHit = "soft_ground"; //Звуки шагов по поверхности
	};
	class maxmapm_Grass2_surface: Default
	{
		files = "mm_grass*";
		rough = 0.1;
		dust = 0.8;
		soundEnviron = "grass";
		character = "maxmapm_Grass_Character";
		soundHit = "soft_ground";
	};
	class maxmapm_Sand_surface: Default
	{
		files = "mm_beach*";
		rough = 0.4;
		dust = 1.0;
		soundEnviron = "sand";
		character = "maxmapm_none_Character";
		soundHit = "soft_ground";
	};
	class maxmapm_Mud_surface: Default
	{
		files = "mm_mud*";
		rough = 1.0;
		dust = 0;
		soundEnviron = "sand";
		character = "maxmapm_none_Character";
		soundHit = "soft_ground";
	};
	class maxmapm_Rock_surface: Default
	{
		files = "mm_rock*";
		rough = 0.4;
		dust = 0;
		soundEnviron = "rock";
		character = "maxmapm_none_Character";
		soundHit = "hard_ground";
	};
};

class CfgSurfaceCharacters //Настройка автоматической расстановки объектов на поверхностях
{
	class maxmapm_Grass_Character
	{
		names[]={"GrassGreen", "GrassBrushHighGreen", "ThistleHigh", "GrassTall", "GrassCrookedDead"}; //Названия обьектов для расстановки из class clutter{}; в основном конфиге
		probability[]={0.4, 0.3, 0.1, 0.1, 0.04}; //Приоритет расстановки обьектов (соответственно) : <=1 в сумме
	};
	class maxmapm_none_Character
	{
		names[]={"GrassGreen"};
		probability[]={0};
	};
};