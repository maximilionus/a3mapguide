//Если локаций на карте < 5, то отображаться на 2D карте они не будут.
//Причем в EDEN редакторе и специальных командах (nearestLocation - https://community.bistudio.com/wiki/nearestLocation) их спокойно видно.
class vill_test_village
{
	name="Test Village";
	position[]={5244.59,6087.05};
	type="NameVillage";
	radiusA=200.00;
	radiusB=200.00;
	angle=0.000;
};
class vill_test_village_2
{
	name="Huh";
	position[]={4244.59,1087.05};
	type="NameVillage";
	radiusA=200.00;
	radiusB=200.00;
	angle=0.000;
};
class vill_test_village_3
{
	name="Deep Web Memes";
	position[]={3244.59,2087.05};
	type="NameVillage";
	radiusA=200.00;
	radiusB=200.00;
	angle=0.000;
};
class vill_test_village_4
{
	name="Dats gut";
	position[]={2244.59,3087.05};
	type="NameVillage";
	radiusA=200.00;
	radiusB=200.00;
	angle=0.000;
};
class vill_test_village_5
{
	name="Awoo";
	position[]={1244.59,4087.05};
	type="NameVillage";
	radiusA=200.00;
	radiusB=200.00;
	angle=0.000;
};